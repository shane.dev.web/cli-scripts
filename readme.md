# install-dev-tools

Web前端开发工具安装脚本。通过命令行脚本，帮助你安装日常开发所需的工具或软件，加速开发环境准备工作效率。

## Installation

如果你无法正常访问github，建议使用[谷歌公开的dns服务器](https://developers.google.com/speed/public-dns/docs/using)

```text
Google Public DNS IP addresses
The Google Public DNS IP addresses (IPv4) are as follows:
8.8.8.8
8.8.4.4

The Google Public DNS IPv6 addresses are as follows:
2001:4860:4860::8888
2001:4860:4860::8844
```

之后，请选择适合你的安装方式。

### 在线安装

- windows系统可以参照[install.example.ps1](./preinstall-dev-environment/install.example.ps1)
- macos系统可以参照[install.example.sh](./preinstall-dev-environment/install.example.sh)

### 下载到本地执行

一般只有在定制安装所需的软件，才可能考虑将安装脚本下载到本地，修改后执行。

首次下载项目中对脚本，在执行前需要留意操作系统对外来脚本执行的安全策略

#### powershell类脚本

先解决charset字符集问题

项目中的脚本内容默认使用utf8编码保存，在windows系统中，你可以采取用记事本打开，
另存为的时候，编码选择"带有BOM的UTF-8"，然后替换保存。

```powershell
cd install-dev-tools
# 首次运行脚本需要手动在powershell中执行下一行代码，然后回答yes即可
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
# 解除锁定。或通过文件属性窗口解锁
Unblock-File ./install-dev-tools-via-scoop.ps1
# 执行脚本
./install-dev-tools-via-scoop.ps1
```

#### bash类脚本

```shell
cd install-dev-tools
# 添加执行权限
chmod +x ./install-dev-tools-via-brew.sh
# 执行脚本
./install-dev-tools-via-brew.sh
```
