:: Manual instalation
:: set ChocolateyInstall=
:: Set-ExecutionPolicy -ExecutionPolicy RemoteSigned
:: iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex

:: Development shell environment
choco install nvm -y
choco install git -y
choco install tortoisegit -y
choco install microsoft-build-tools -y
:: choco install wamp-server -y --dir "d:\wamp"

:: IDE/Editor
choco install notepadplusplus.install -y
choco install webstorm -y
choco install phpstorm -y
choco install atom -y
:: choco install visualstudio2015community -y

:: Helpefull Tools
choco install fiddler -y
choco install beyondcompare -y
choco install sourcetree -y
choco install googlechrome -y
choco install psiphon -y
choco install 7zip -y
choco install shadowsocks -y

:: Virtual Machine
choco install virtualbox -y
choco install vagrant -y
:: choco install vmwareworkstation -y --ignore-checksums

:: Useful software
choco install wechat -y --ignore-checksums
choco install foxitreader -y
choco install ynote -y
:: choco install office365proplus -y
:: choco install kindle -y --ignore-checksums

echo.
echo please install manually visualstudio2015community, vmwareworkstation, office365proplus!
pause
