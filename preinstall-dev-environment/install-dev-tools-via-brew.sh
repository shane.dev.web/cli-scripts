echo '请输入一次性授权密码，后续不在有密码输入提示'
# shellcheck disable=SC2162
read -s UserPass
originSudo=$(which sudo)
sudo() {
  echo "$UserPass" | $originSudo -S -p '' "$*"
}
echo '已经完成授权，后续不再有密码输入提示。现在无需值守在电脑旁，你可以去做点其他事情了'

# 尝试处理由dns污染造成的github无法访问的问题
IsCensoredNetwork=false
if curl -L https://raw.githubusercontent.com &> /dev/null; then
  echo '能够正常访问 https://raw.githubusercontent.com'
else
  IsCensoredNetwork=true
  echo '尝试设置公开的Google DNS服务器IPv4地址'
  sudo networksetup -setdnsservers Wi-Fi 8.8.8.8 8.8.4.4
  sudo networksetup -setdnsservers Ethernet 8.8.8.8 8.8.4.4
fi

## 如果以下命令无法使用，请手动安装xcode cli tools。
#sudo xcode-select --install
if xcode-select -p &> /dev/null; then
  echo "Xcode CLI tools 已安装"
else
  echo "Xcode CLI tools 未找到. 尝试安装..."
  # create the placeholder file that's checked by CLI updates' .dist code
  # in Apple's SUS catalog
  clt_placeholder="/tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress"
  touch $clt_placeholder
  # find the CLI Tools update
  clt_label_command=$(softwareupdate -l | grep "\*.*Command Line Tools for" | tail -n 1 | sed 's/^[^C]*//')
  # install it
  softwareupdate -i "$clt_label_command" --verbose
  rm $clt_placeholder
fi

## 安装homebrew https://docs.brew.sh/Installation
if command -v brew > /dev/null; then
  echo brew已经安装
else
  # https://docs.brew.sh/Installation#unattended-installation
  sudo -v
  NONINTERACTIVE=1 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

brew install --cask keka # 跟7z同系的压缩解压软件

brew install git
brew install git-lfs
git lfs install

# node开发相关
# 安装volta https://volta.sh/ 替代nvm
brew install volta
volta setup
# 重载环境变量
# shellcheck source=${HOME}/.zshrc
source ~/.zshrc
#$SHELL
# 安装nvm https://github.com/nvm-sh/nvm#install--update-script
#if command -v nvm > /dev/null
#then
#  echo nvm已经安装
#else
#  touch ~/.zshrc
#  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
#  source ~/.zshrc
#fi
# 安装node
volta install node
#nvm install node
#nvm install --lts
#nvm install --lts=Dubnium
# 安装yarn
volta install yarn
#npm i -g yarn
# 在国内开发可能需要设置这个，在安装脚本结束后自行设置，作为平常使用的源
if $IsCensoredNetwork; then
  npm config set registry https://registry.npm.taobao.org
  yarn config set registry https://registry.npm.taobao.org
fi
# todo 多个node版本共享npm config prefix

## java sdk
#brew install --cask adoptopenjdk
## python
#brew install python

# web前端开发相关
# IDE编辑器
brew install --cask visual-studio-code
#brew install --cask webstorm
# 浏览器
brew install --cask google-chrome
# http请求调试工具
brew install --cask charles
# 移动web应用开发相关
# iphone命令行工具，可以提供ipa安装另一种途径
#brew install libimobiledevice ideviceinstaller
# android 命令行工具，包含 adb
#brew install --cask android-platform-tools android-file-transfer
## android sdk也可以通过android studio安装 https://gist.github.com/patrickhammond/4ddbe49a67e5eb1b9c03
# brew install --cask android-studio

# 图形工具
brew install drawio

# 通讯应用
brew install --cask slack
#brew install --cask telegram
#brew install --cask wechat

# 容器和虚拟机
#brew install --cask docker
# Parallel desktop https://www.google.com/search?q=Parallels+Desktop+site%3Arutracker.org
#brew install --cask parallels

# 操作系统下载 https://www.google.com/search?q=mac+iso+site%3Arutracker.org
# office https://www.google.com/search?q=mac+office+site%3Arutracker.org

# 有道词典
#brew install --cask youdaodict
# 下载工具
#brew install --cask folx
# onedrive云盘
#brew install --cask onedrive
# vpn客户端
#brew install --cask shadowsocksx

echo '你依然可以根据个人需要去brew官方应用仓库 https://brew.sh/ 进行搜索应用，然后通过搜索结果中的命令行帮助信息安装应用。'

# 卸载应用示例
# brew uninstall --zap multipass # to destroy all data, too
