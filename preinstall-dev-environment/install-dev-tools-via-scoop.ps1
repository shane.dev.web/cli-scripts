# 临时使用中文页码，让命令行支持显示中文 https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/chcp
chcp 936 | Out-Null

# Note: if you get an error you might need to change the execution policy (i.e. enable Powershell) with Set-ExecutionPolicy RemoteSigned -scope CurrentUser
# 如果是首次在本地运行脚本需要手动在powershell中执行下一行代码，然后回答yes即可
# Set-ExecutionPolicy RemoteSigned -scope CurrentUser -force
# 如果还遇到类似的错误，可能是你脚本文件是从网络上下载，被windows锁定了，需要从文件属性窗口中解锁

# 另起线程执行命令函数，可用scoop install sudo来简化安装脚本
function RunCommands ($arrCommands, [bool]$privilege = $false, [bool]$wait = $false) {
  $command = 'Start-Process', '-FilePath powershell'
  if ($wait) {
    $command += '-Wait'
    $arrCommands += ";'Wait for 3 seconds to exit'"
    $arrCommands += ';Start-Sleep -Seconds 3'
  }
  if ($privilege) {
    $command += '-Verb RunAs'
  }
  $arrCommands = $arrCommands -join ';'
  $command += "-ArgumentList ""$arrCommands"""
  $command = $command -join " "
  Write-Host "将执行命令：$command"
  Invoke-Expression $command
}

# 添加目录到path环境变量
function AddToPath {
  param(
    [string]$Dir
  )
  # 检查路径是否真实存在
  # if ( !(Test-Path $Dir) ) {
  #   Write-warning "Supplied directory was not found!"
  #   return
  # }
  $PATH = [Environment]::GetEnvironmentVariable("PATH")
  if ( $PATH -notlike "*" + $Dir + "*" ) {
    # 添加到当前运行时环境变量
    [Environment]::SetEnvironmentVariable("PATH", "$PATH;$Dir")
    # 永久保存到用户环境变量配置中
    [Environment]::SetEnvironmentVariable("PATH", "$PATH;$Dir", "User")
  }
}

# 重新读取用户+系统path环境变量
function ReloadEnvPath {
  # refreshing env in current shell
  $env:path = [System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path", "User")
}

# MessageBox依赖的GUI框架
#Add-Type -AssemblyName 'PresentationFramework'
#[System.Windows.MessageBox]::Show('安装期间可能会弹出 UAC 确认对话框，请点击允许协助完成安装')
Write-Warning '
安装期间会弹出 UAC 操作授权确认对话框，请允许以协助完成安装。

如果你在 UAC 操作授权确认对话框中，点击 "是" 以后，发现powershell窗口没有继续下一步动作（执行进程被UAC交互冻结），请选中powershell窗口，按 esc 键继续。

如果你后续看到无需值守的文本提示，便可离开做其他的事情，脚本会自己完成剩下的事情。
'
Read-Host -Prompt "按回车键继续"

# 尝试处理由dns污染造成的github无法访问的问题
$IsCensoredNetwork = $false
try {
  Invoke-WebRequest https://raw.githubusercontent.com -UseBasicParsing | Out-Null
  # 'Scoop installation script can be downloaded. No need to change DNS.'
  '能够正常访问 https://raw.githubusercontent.com'
} catch {
  $IsCensoredNetwork = $true
  $ActiveInterface = Get-NetAdapter | Where-Object {$_.Status -EQ 'Up' -and $_.Name -Match 'Ethernet|Wi-Fi|本地连接'}
  #$ActiveInterface
  if ($ActiveInterface.Count -gt 1) {
    $ActiveInterface
    # Please enter your active network interface index above manually since it help your setup DNS
    $InterfaceIndex = Read-Host "你的计算机有多个用于联网的网络适配器（Ethernet|Wi-Fi|本地连接），请手动输入你当前正在使用的主要网络适配器 ifIndex 索引数字，以协助DNS设置程序完成设置。"
  } else {
    $InterfaceIndex = $ActiveInterface.ifIndex
  }
  #$InterfaceIndex
  #runCommands "Set-DnsClientServerAddress -InterfaceIndex $InterfaceIndex -ResetServerAddresses" $true
  $GoogleDNSServersIPv4 = '8.8.8.8','8.8.4.4'
  Write-Warning "为当前正在使用的网络适配器设置谷歌公开的dns服务器地址 $GoogleDNSServersIPv4，以解决无法访问github的问题，该操作需要UAC授权"
  RunCommands "Set-DnsClientServerAddress -InterfaceIndex $InterfaceIndex -ServerAddresses ('$($GoogleDNSServersIPv4 -join "','")')" $true $true
  #runCommands "Set-DNSClientServerAddress -InterfaceIndex $InterfaceIndex –ServerAddresses ('2001:4860:4860::8888','2001:4860:4860::8844')" $true
  if ((Get-DnsClientServerAddress -AddressFamily IPv4 -InterfaceIndex $InterfaceIndex).ServerAddresses[0] -eq $GoogleDNSServersIPv4[0]) {
    Write-Host "公开的dns服务器IPv4地址 $GoogleDNSServersIPv4 设置成功" -ForegroundColor DarkGreen
  } else {
    Write-Error "设置失败，请考虑手动设置网络适配器的 DNS IPv4 地址 $GoogleDNSServersIPv4"
    Read-Host -Prompt "按回车键继续"
    Exit
  }
}

# 安装scoop
if (Get-Command "scoop" -errorAction SilentlyContinue) {
  #"scoop is already installed"
  'scoop已经安装'
} else {
  #Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
  Set-ExecutionPolicy RemoteSigned -s cu -force
  Invoke-RestMethod get.scoop.sh | Invoke-Expression
}

# sudo用于提权
scoop install gsudo
#PowerShell -Command "Set-ExecutionPolicy RemoteSigned -scope Process; [Net.ServicePointManager]::SecurityProtocol = 'Tls12'; iwr -useb https://raw.githubusercontent.com/gerardog/gsudo/master/installgsudo.ps1 | iex"
# 重载path环境变量
#reloadEnvPath
# 一次性提权，后续gsudo相关不在弹出UAC提示框，直到退出
gsudo cache on --duration '-1'
Write-Host '已经完成UAC授权，后续不再弹出UAC提示框。现在无需值守在电脑旁，你可以去做点其他事情了' -ForegroundColor Green
#Write-Host '后续不再弹出UAC提示框。现在无需值守在电脑旁，你可以去做点其他事情了' -ForegroundColor Green

if (Get-Command "git" -errorAction SilentlyContinue) {
  #"git is already installed"
  'git已经安装'
} else {
  # 后续添加bucket，会依赖git，git会安装7zip
  scoop install git
  scoop install git-lfs
  git lfs install
  # 添加7zip鼠标右键上下文菜单
  reg import "$env:HOMEPATH\scoop\apps\7zip\current\install-context.reg"
}

# 修复main bucket不是有效git项目的bug https://github.com/ScoopInstaller/Scoop/issues/4917#issuecomment-1125400640
# 可以在新版发布后移除
scoop bucket rm main
scoop bucket add main

# node开发相关
# 安装volta https://volta.sh/ 替代nvm
if (Get-Command "volta" -errorAction SilentlyContinue) {
  'volta已经安装'
} else {
  scoop bucket add extras
  gsudo scoop install vcredist-aio # vcredist runtime 新版volta依赖vcruntime140
  scoop bucket add main
  scoop install volta
}
# 安装nvm https://github.com/coreybutler/nvm-windows
#if (Get-Command "nvm" -errorAction SilentlyContinue) {
#  #"nvm is already installed"
#  'nvm已经安装'
#}
#else {
#  scoop bucket add main
#  scoop install nvm
#}
# 安装node
#  # $node_version = "10.24.0"
#  # nvm install latest
#  nvm install lts
#  #nvm install $node_version
#  # 会弹出 UAC 确认对话框
#  gsudo nvm use lts
#  # nvm use $node_version
volta install node
# 安装yarn
if (Get-Command "yarn -v" -errorAction SilentlyContinue) {
  #"yarn is already installed"
  'yarn已经安装'
} else {
  #scoop install yarn
  volta install yarn
}
# 在国内开发可能需要设置这个，在安装脚本结束后自行设置，作为平常使用的源
if ($IsCensoredNetwork) {
  npm config set registry https://registry.npm.taobao.org
  yarn config set registry https://registry.npm.taobao.org
}
# 给全局npm包安装，设置统一安装目录。用nvm切换node版本就不会受到影响，以前的全局npm包就还可以继续使用
#$global_npm_packages_path = "$env:NVM_HOME\packages"
#npm config set prefix "$global_npm_packages_path"
#yarn config set prefix "$global_npm_packages_path"
#AddToPath "$global_npm_packages_path"
# 某些npm包所需的编译环境。需要用管理员权限安装的包，会弹出 UAC 确认对话框
# Node.js now includes build tools for Windows. You probably no longer need this tool. See https://github.com/felixrieseberg/windows-build-tools for details.
#gsudo npm i -g windows-build-tools

# java相关
#scoop bucket add java
#scoop install openjdk

# web前端开发相关
# IDE编辑器
scoop bucket add extras
scoop install vscode
#scoop install webstorm
# 浏览器
scoop bucket add extras
scoop install googlechrome
# http请求调试工具
scoop bucket add chawyehsu_dorado https://github.com/chawyehsu/dorado
scoop install charles
# 移动web应用开发相关
#scoop bucket add main
#scoop install adb
#scoop bucket add extras
#scoop install ios-webkit-debug-proxy

# 图形工具
scoop install draw.io

# 通讯应用
scoop bucket add extras
scoop install slack
# scoop install telegram
#scoop bucket add ivaquero_scoopet https://github.com/ivaquero/scoopet
#scoop install wechat

# 容器和虚拟机
#scoop bucket add main
#scoop install docker
#scoop bucket add ivaquero_scoopet https://github.com/ivaquero/scoopet
#scoop install vmware-workstation-pro

# 操作系统下载
#scoop bucket add extras
#scoop install windows-iso-downloader
# officetoolplus 用来安装office批量激活版
scoop bucket add batkiz_backit https://github.com/batkiz/backit
scoop install office-tool-plus-with-runtime
Write-Host 'office-tool-plus如何使用可参见官方文档 https://otp.landian.vip/zh-cn/' -ForegroundColor Gray
#scoop bucket add manjaroyyq_Meta https://github.com/manjaroyyq/Meta
#scoop install heu-kms-activator

# 有道词典
#scoop bucket add dodorz_scoop https://github.com/dodorz/scoop
#scoop install YoudaoDict
# 下载工具
#scoop bucket add kkzzhizhou_scoop-zapps https://github.com/kkzzhizhou/scoop-zapps
#scoop install ThunderS
# vpn客户端
#scoop bucket add extras
#scoop install shadowsocks

# FAQ 常见问题
# 快捷方式丢失，可以使用如下命令恢复 https://github.com/lukesampson/scoop/wiki/FAQ#how-do-i-switch-between-different-versions-of-an-app
# scoop reset *

Write-Host '你依然可以根据个人需要去scoop官方应用仓库 https://scoop.sh/#/apps?s=0&d=1&o=false 进行搜索应用，然后通过搜索结果中的命令行帮助信息添加bucket和安装应用。' -ForegroundColor Gray
